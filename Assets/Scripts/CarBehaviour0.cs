﻿using UnityEngine;
using System.Collections;

public class CarBehaviour0 : CarBehaviour {
	void Update()
	{
		//Read sensor values
		float leftSensor = LeftLD.getGaussianOutput ();
		float rightSensor = RightLD.getGaussianOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = rightSensor * MaxSpeed;
		m_RightWheelSpeed = leftSensor * MaxSpeed;
	}
}
