﻿using UnityEngine;
using System.Collections;

public class CarBehaviourPark : CarBehaviour {
	void Update()
	{
		//Read sensor values
		float leftLightSensor = LeftLD.getLinearOutput ();
		float rightLightSensor = RightLD.getLinearOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = rightLightSensor * MaxSpeed;
		m_RightWheelSpeed = leftLightSensor * MaxSpeed;

		GameObject[] leftLights = LeftLD.GetVisibleCloseLights ();
		foreach (GameObject light in leftLights) { light.SetActive (false); }
		GameObject[] rightLights = RightLD.GetVisibleCloseLights ();
		foreach (GameObject light in rightLights) { light.SetActive (false); }
	}
}
