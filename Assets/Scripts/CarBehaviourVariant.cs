﻿using UnityEngine;
using System.Collections;

public class CarBehaviourVariant : CarBehaviour {
	void Update()
	{
		//Read sensor values
		float leftBlockSensor = LeftBD.getGaussianOutput ();
		float rightBlockSensor = RightBD.getGaussianOutput ();
		float leftLightSensor = LeftLD.getGaussianOutput ();
		float rightLightSensor = RightLD.getGaussianOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = (leftLightSensor + leftBlockSensor) * MaxSpeed;
		m_RightWheelSpeed = (rightLightSensor + rightBlockSensor) * MaxSpeed;
	}
}
