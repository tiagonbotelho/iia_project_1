﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class LightDetectorScript : MonoBehaviour {

	public float angle;
	public float output;
	public int numObjects;
	public float mean;
	public float stdev;
	public float bias;
	public float max;
	public float min;
	public float limiar_sup;
	public float limiar_inf;

	void Start ()
	{
		output = 0;
		numObjects = 0;
	}

	void FixedUpdate ()
	{
		GameObject[] lights = GetVisibleLights ();
		output = 0;
		numObjects = lights.Length;

		foreach (GameObject light in lights) {
			float r = light.GetComponent<Light> ().range;
			output +=  1f/ Mathf.Pow((transform.position - light.transform.position).magnitude / r + 1, 2);
			Debug.DrawLine(transform.position, light.transform.position, Color.green);
		}

		if (numObjects > 0) {
			output = output / numObjects;
		}
	}

	public float getLinearOutput()
	{
		return output;
	}

	public float getGaussianOutput()
	{
		float result;
		if (output < limiar_inf) {
			result = min;
		} else if (output > limiar_sup) {
			result = min;
		} else {
			result = (1f / (Mathf.Sqrt(2f * Mathf.PI) * stdev) * Mathf.Exp((float) - Math.Pow((output - mean), 2f) / (float)(2f * Math.Pow(stdev,2F))));
			result *= bias;

			if (result > max) {
				return max;
			} else if (result < min) {
				return min;
			}
		}
		return result;
	}

	// Returns all "Light" tagged objects. The sensor angle is not taken into account.
	GameObject[] GetAllLights()
	{
		return GameObject.FindGameObjectsWithTag ("Light");
	}
		
	// Returns all "Light" tagged objects that are within the view angle of the Sensor. Only considers the angle over 
	// the y axis. Does not consider objects blocking the view.
	GameObject[] GetVisibleLights()
	{
		ArrayList visibleLights = new ArrayList();
		float halfAngle = angle / 2.0f;

		GameObject[] lights = GameObject.FindGameObjectsWithTag ("Light");

		foreach (GameObject light in lights) {
			Vector3 toVector = (light.transform.position - transform.position);
			Vector3 forward = transform.forward;
			toVector.y = 0;
			forward.y = 0;
			float angleToTarget = Vector3.Angle (forward, toVector);

			if (angleToTarget <= halfAngle) {
				visibleLights.Add (light);
			}
		}

		return (GameObject[])visibleLights.ToArray(typeof(GameObject));
	}

	public GameObject[] GetVisibleCloseLights()
	{
		ArrayList visibleLights = new ArrayList();
		float halfAngle = angle / 2.0f;

		GameObject[] lights = GameObject.FindGameObjectsWithTag ("Light");

		foreach (GameObject light in lights) {
			Vector3 toVector = (light.transform.position - transform.position);
			Vector3 forward = transform.forward;
			toVector.y = 0;
			forward.y = 0;
			float angleToTarget = Vector3.Angle (forward, toVector);

			if (angleToTarget <= halfAngle && toVector.magnitude < 2) {
				visibleLights.Add (light);
			}
		}

		return (GameObject[])visibleLights.ToArray(typeof(GameObject));
	}
}
