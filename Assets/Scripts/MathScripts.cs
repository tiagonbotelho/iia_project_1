﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class MathScripts {
	public static float pi = (float)Math.PI;

	public MathScripts () { }

	/* LINK: https://en.wikipedia.org/wiki/Gaussian_function */
	public float GaussianCalc(float x, float stdev, float mean) {
		return (1F/(Mathf.Sqrt(2F*Mathf.PI)*stdev)*Mathf.Exp((float)-Math.Pow((x - mean),2F)/(float)(2F*Math.Pow(stdev,2F))));
	}

	/* Calculates the value for the linear function */
	public float LinearCalc(float x) {
		return 1f / Mathf.Pow (x, 2);
	}
}

