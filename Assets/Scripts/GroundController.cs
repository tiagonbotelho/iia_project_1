﻿using UnityEngine;
using System.Collections;

public class GroundController : MonoBehaviour {

	int counter = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown ()
	{      
		counter ++;
		var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		GameObject lightGameObject = new GameObject("Light " + counter.ToString());
		lightGameObject.tag = "Light";
		Light light = lightGameObject.AddComponent<Light>();
		light.color = Color.yellow;
		light.range = 5;
		light.intensity = 3;
		light.bounceIntensity = 2;
		lightGameObject.transform.position = new Vector3(position.x, 1, position.z);
	}
}
