﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class BlockDetectorScript : MonoBehaviour {

	public float angle;
	public float output;
	public int numObjects;
	public float mean;
	public float stdev;
	public float bias;
	public float min;
	public float max;
	public float limiar_sup;
	public float limiar_inf;

	void Start ()
	{
		output = 0;
		numObjects = 0;
	}

	void FixedUpdate ()
	{
		GameObject block = GetNearestVisibleBlock ();

		if (block != null) {
			output = 1f / ((transform.position - block.transform.position).magnitude + 1);
			Debug.DrawLine(transform.position, block.transform.position, Color.cyan);
		}
	}

	public float getLinearOutput()
	{
		return output;
	}

	public float getGaussianOutput()
	{
		float result;
		if (output < limiar_inf) {
			result = min;
		} else if (output > limiar_sup) {
			result = min;
		} else {
			result = (1f / (Mathf.Sqrt(2f * Mathf.PI) * stdev) * Mathf.Exp((float) - Math.Pow((output - mean), 2f) / (float)(2f * Math.Pow(stdev,2F))));
			result *= bias;

			if (result > max) {
				return max;
			} else if (result < min) {
				return min;
			}
		}
		return result;
	}
		
	// Returns all "Block" tagged objects that are within the view angle of the Sensor. Only considers the angle over 
	// the y axis. Does not consider objects blocking the view.
	GameObject GetNearestVisibleBlock()
	{
		float minDistance = -1f;
		GameObject closestBlock = null;
		float halfAngle = angle / 2.0f;

		GameObject[] blocks = GameObject.FindGameObjectsWithTag ("Block");

		foreach (GameObject block in blocks) {

			Vector3 toVector = (block.transform.position - transform.position);
			Vector3 forward = transform.forward;
			toVector.y = 0;
			forward.y = 0;
			float angleToTarget = Vector3.Angle (forward, toVector);
			float distance = Vector3.Distance (block.transform.position, transform.position);
			
			if ((minDistance == -1 || minDistance > distance) && angleToTarget <= halfAngle) {
				closestBlock = block;
				minDistance = distance;
			}
		}

		return closestBlock;
	}
}
