﻿using UnityEngine;
using System.Collections;

public class CarBehaviour2b : CarBehaviour {
	void Update()
	{
		//Read sensor values
		float leftSensor = LeftLD.getLinearOutput ();
		float rightSensor = RightLD.getLinearOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = rightSensor * MaxSpeed;
		m_RightWheelSpeed = leftSensor * MaxSpeed;
	}
}
