﻿using UnityEngine;
using System.Collections;

public class CarBehaviour3a : CarBehaviour {
	void Update()
	{
		//Read sensor values
		float leftBlockSensor = LeftBD.getLinearOutput ();
		float rightBlockSensor = RightBD.getLinearOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = leftBlockSensor * MaxSpeed;
		m_RightWheelSpeed = rightBlockSensor * MaxSpeed;
	}
}
