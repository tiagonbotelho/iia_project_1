﻿using UnityEngine;
using System.Collections;

public class CarBehaviour3b : CarBehaviour {
	void Update()
	{
		//Read sensor values
		float leftSensor = LeftBD.getLinearOutput ();
		float rightSensor = RightBD.getLinearOutput ();

		//Calculate target motor values
		m_LeftWheelSpeed = rightSensor * MaxSpeed;
		m_RightWheelSpeed = leftSensor * MaxSpeed;
	}
}
